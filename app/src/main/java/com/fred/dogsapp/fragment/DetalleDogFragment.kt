package com.fred.dogsapp.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.fred.dogsapp.R
import com.fred.dogsapp.entity.DogEntity
import kotlinx.android.synthetic.main.fragment_detalle_dog.*


class DetalleDogFragment : Fragment() {

    private lateinit var dogEntity: DogEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            dogEntity = it.getParcelable("dogEntity")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detalle_dog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        toolbar.setNavigationIcon(R.drawable.ic_back)
        txtRaza.text = dogEntity.raza
        txtDescipcion.text = dogEntity.description
        txtTamanio.text = dogEntity.tamanio
        txtRangoEdad.text = dogEntity.rangoEdad
        txtEspecialidad.text = dogEntity.especialidad
        imgDog.setImageResource(dogEntity.imagen)
        root.setBackgroundColor(ContextCompat.getColor(requireContext(), dogEntity.background))

        toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }
}