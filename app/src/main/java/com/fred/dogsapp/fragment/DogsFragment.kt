package com.fred.dogsapp.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fred.dogsapp.R
import com.fred.dogsapp.Utilitary
import com.fred.dogsapp.adapter.DogAdapter
import com.fred.dogsapp.entity.DogEntity
import kotlinx.android.synthetic.main.fragment_bienvenida.*
import kotlinx.android.synthetic.main.fragment_dogs.*


class DogsFragment : Fragment(), DogAdapter.OnDogClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dogs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        loadDogs()
    }

    private fun setupRecyclerView(){
        rv_dog.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun loadDogs(){
        rv_dog.adapter = DogAdapter(requireContext(),Utilitary.getListOfDogs(),this)
    }

    override fun OnDogClick(dogs: DogEntity, position: Int) {
        val bundle = Bundle()
        bundle.putParcelable("dogEntity", dogs)

        findNavController().navigate(R.id.nav_dog_fragment, bundle)

    }

}