package com.fred.dogsapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.fred.dogsapp.R
import com.fred.dogsapp.base.BaseViewHolder
import com.fred.dogsapp.entity.DogEntity
import java.text.FieldPosition

class DogAdapter (private val context: Context, private val dogList: List<DogEntity>
    private val itemLongClickListener:OnDogClickListener) : RecyclerView.Adapter<BaseViewHolder<*>>()
{

    interface OnDogClickListener{
        fun OnDogClick (dogs:DogEntity, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return MainViewHolder(
            LayoutInflater.from(context).inflate(R.layout.dogs_row, parent,attachToRoot:false)
        )
    }

    override fun getItemCount(): Int {
        return dogList.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        TODO("Not yet implemented")
    }

    inner class MainViewHolder(itemView: View):BaseViewHolder<DogEntity>(itemView){
        override fun bind(item: DogEntity, position:Int){

        }
    }
}